@if(count($items)>0)
    <div class="section" style="padding-bottom:100px">
        <div class="container">
            <div class="google_font offerTitle" style="font-family:'Kanit',Arial,Tahoma,sans-serif;font-size:40px;line-height:90px;font-weight:900;letter-spacing:0px;color:#1955ff;text-align: center">
                {!! $fields->head2 !!}
            </div>
            <div class="row text-center justify-content-center">
                @foreach($items as $item)
                    <div class="col-sm-6 col-md-4 col-lg-3 custom_hover_color mb-4">
                        <div class="hover_color align_" style="background-color:#e0e1e5;" ontouchstart="this.classList.toggle('hover');">
                            <div class="hover_color_bg" style="background-color:#ebeeee;border-width:0px;">
                                <a href="#">
                                    <div class="hover_color_wrapper" style="padding:45px 30px 30px;">
                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="{{renderImage($item->galleryCover(), 45, 45, 'fit')}}" alt="mechanic7-about-icon1" title="" width="45" height="45" /> </div>
                                        </div>
                                        <hr class="no_line" style="margin: 0 auto 10px auto" />
                                        <h6 style="color:#252525; letter-spacing:0; line-height: 1.1; text-transform: uppercase; font-weight: 500">{!! $item->title!!}</h6> </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
