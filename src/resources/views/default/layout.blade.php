<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {!! SEOMeta::generate() !!}

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('image/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('image/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('image/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('image/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('image/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('image/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('image/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('image/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('image/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('image/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('image/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('image/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('image/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('image/favicon//manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel='stylesheet'
          href='https://fonts.googleapis.com/css?family=Kanit:100,200,300,400,400italic,500,600,700,700italic,900'>
    <link rel='stylesheet'
          href='https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,400italic,500,600,700,700italic,900'>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel='stylesheet' href="{{asset('content/mechanic7/css/structure.css')}}">
    <link rel='stylesheet' href="{{asset('content/mechanic7/css/mechanic7.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/rs-plugin-6.custom/css/rs6.css')}}">
</head>

<link href="{{asset('css/main.css')}}" rel="stylesheet">

<script>
    const BASE_URL = '{{url()->to('/')}}/';
    const CSRF_TOKEN = '{{csrf_token()}}';
    const SITE_LANG = '{{app()->getLocale()}}';
</script>

@stack('scrips.head.bottom')
</head>
<body
    class="home page template-slider button-custom layout-full-width one-page if-zoom if-border-hide no-shadows header-transparent sticky-header sticky-tb-color ab-show subheader-both-center menu-link-color menuo-right menuo-no-borders logo-no-margin mobile-tb-center mobile-side-slide mobile-mini-mr-lc tablet-sticky mobile-header-mini mobile-sticky">

<div id="Wrapper">
    <div id="Header_wrapper">
        <header id="Header">
            <div id="Top_bar">
                <div class="container">
                    <div class="column one">
                        <div class="top_bar_left clearfix">
                            <div class="menu_wrapper">
                                <nav id="menu">
                                    @include('default.nav_item.main', ['name' => 'main'])
                                </nav>
                                <a class="responsive-menu-toggle" href="#"><i class="icon-menu-fine"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('slider')
        </header>
    </div>
    <div id="Content">
        @yield('content')
    </div>
    <footer id="Footer" style=" border-top: 1px solid #1955ff " class="clearfix">
        <div class="footer_copy">
            <div class="container">

                <div class="copyright" style="text-align: center; width: 100%; line-height: 3"> Strona stworzona przez:
                    <a target="_blank" rel="nofollow" href="https://www.palmax.com.pl">Palmax</a></div>

            </div>
        </div>
    </footer>
</div>
<div id="Side_slide" class="right light" data-width="250">
    <div class="close-wrapper"> <a href="#" class="close"><i class="icon-cancel-fine"></i></a> </div>
    <div class="extras">
        <div class="extras-wrapper"></div>
    </div>
    <div class="menu_wrapper"></div>
</div>
<div id="body_overlay"></div>


<script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('js/mfn.menu.js')}}"></script>
<script src="{{asset('js/jquery.plugins.js')}}"></script>
<script src="{{asset('js/jquery.jplayer.min.js')}}"></script>
<script src="{{asset('js/animations/animations.js')}}"></script>
<script src="{{asset('js/translate3d.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{asset('plugins/rs-plugin-6.custom/js/revolution.tools.min.js')}}"></script>
<script src="{{asset('plugins/rs-plugin-6.custom/js/rs6.min.js')}}"></script>
<script src="{{asset('phpmailer/form.js')}}"></script>

<script src="{{asset('js/frontend.js')}}"></script>
<script src="{{asset('js/main.min.js')}}"></script>

@stack('scripts.body.bottom')
</body>
</html>
