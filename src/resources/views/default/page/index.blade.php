@extends('default.layout')
@section('slider')
    @if(isset($page->gallery->items))
        <div class="mfn-main-slider mfn-rev-slider">
            <!-- START Home Tea4 REVOLUTION SLIDER 6.4.3 -->
            <p class="rs-p-wp-fix"></p>
            <rs-module-wrap id="rev_slider_1_1_wrapper" data-source="gallery"
                            style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
                <rs-module id="rev_slider_1_1" data-version="6.4.3">
                    <rs-slides>
                        @foreach($page->gallery->items as $key=>$item)
                            <rs-slide data-key="rs-{{$key+1}}" data-title="Slide" data-in="o:0;" data-out="a:false;">
                                <img
                                    src="{{renderImage($item->url, 1920, 900, 'fit')}}" class="rev-slidebg">
                                <rs-layer id="slider-1-slide-{{$key+1}}-layer-2" data-type="text" data-color="#ffffff"
                                          data-rsp_ch="on"
                                          data-xy="x:c,c,c,c;xo:0,0,0,0;y:c,c,c,c;yo:0,0,0,0;"
                                          data-text="w:normal;s:70,60,45,70;l:80,90,60,70;fw:900;a:center,center,center,center;"
                                          data-dim="w:1000px,900px,650px,650px;" data-frame_0="x:-50,-50,-28,-28;"
                                          data-frame_1="sp:1000;" data-frame_999="o:0;st:w;"
                                          style="z-index:10;font-family:Kanit;"> {!! str_replace(['<p>', '</p>'], '', $item->text) !!}
                                </rs-layer>
                                <rs-layer id="slider-1-slide-{{$key+1}}-layer-4" data-type="object" data-rsp_ch="on"
                                          data-xy="xo:50px,50px,28px,28px;yo:210px,210px,120px,120px;"
                                          data-text="w:normal;s:20,20,11,11;l:0,0,13,13;" data-frame_999="o:0;st:w;"
                                          style="z-index:11;font-family:Roboto;"></rs-layer>
                            </rs-slide>
                        @endforeach
                    </rs-slides>
                </rs-module>
            </rs-module-wrap>
        </div>

        @push('scripts.body.bottom')
            <script type="text/javascript">
                var revapi1, tpj;

                function revinit_revslider11() {
                    jQuery(function () {
                        tpj = jQuery;
                        revapi1 = tpj("#rev_slider_1_1");
                        if (revapi1 == undefined || revapi1.revolution == undefined) {
                            revslider_showDoubleJqueryError("rev_slider_1_1");
                        } else {
                            revapi1.revolution({
                                sliderLayout: "fullwidth",
                                visibilityLevels: "1240,1240,778,778",
                                gridwidth: "1360,1360,778,778",
                                gridheight: "940,940,960,960",
                                spinner: "spinner14",
                                duration: "2500",
                                perspective: 600,
                                perspectiveType: "global",
                                spinnerclr: "#1955ff",
                                editorheight: "940,768,960,720",
                                responsiveLevels: "1240,1240,778,778",
                                progressBar: {
                                    disableProgressBar: true
                                },
                                navigation: {
                                    onHoverStop: false,
                                    bullets: {
                                        enable: true,
                                        tmp: "",
                                        style: "hermes",
                                        hide_onmobile: true,
                                        hide_under: "768px",
                                        h_align: "right",
                                        v_align: "center",
                                        h_offset: 30,
                                        v_offset: 0,
                                        direction: "vertical",
                                        space: 15
                                    }
                                },
                                fallbacks: {
                                    allowHTML5AutoPlayOnAndroid: true
                                },
                            });
                        }
                    });
                } // End of RevInitScript
                var once_revslider11 = false;
                if (document.readyState === "loading") {
                    document.addEventListener('readystatechange', function () {
                        if ((document.readyState === "interactive" || document.readyState === "complete") && !once_revslider11) {
                            once_revslider11 = true;
                            revinit_revslider11();
                        }
                    });
                } else {
                    once_revslider11 = true;
                    revinit_revslider11();
                }
            </script>
        @endpush
    @endif
@endsection

@section('content')

    <div class="section" style="padding-top:40px">

    </div>
    <div class="section"
         style="padding-top:100px; padding-bottom: 100px; background-image:url({{asset('content/mechanic7/images/mechanic7-section-bg7.png')}});background-repeat:no-repeat;background-position:right top">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="padding:0 2% 0 0">
                    <div class="image_frame image_item no_link scale-with-grid no_border">
                        <div class="image_wrapper"><img class="scale-with-grid"
                                                        src="{{asset('content/mechanic7/images/mechanic7-about-pic1.png')}}"
                                                        alt="mechanic7-about-pic1" title="" width="780" height="953"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="padding:10% 2%">
                    <div class="row">
                        <div class="col-12">
                            <div class="mobile_align_center">
                                <div class="google_font"
                                     style="font-family:'Kanit',Arial,Tahoma,sans-serif;font-size:90px;line-height:90px;font-weight:900;letter-spacing:0px;color:#1955ff;"> {!! $fields->head1 !!} </div>
                                <hr class="no_line" style="margin: 0 auto 15px auto"/>
                                <h3>{{getConstField('company_name')}}</h3>
                                <hr class="no_line" style="margin: 0 auto 15px auto"/>
                                {!! $fields->text1 !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('default.offer.home')

    <div class="section" style="padding-top:140px;padding-bottom:100px;background-color:#17181b" data-parallax="3d"><img
            class="mfn-parallax" src="{{asset('content/mechanic7/images/mechanic7-section-bg4.jpg')}}" alt="parallax background"
            style="opacity:0"/>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="mobile_align_center">
                        <h2 style="color:#ecedf0; text-align: center">{!! $fields->head3 !!}</h2>
                        <hr class="no_line" style="margin: 0 auto 80px auto"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="gallery"
         style="padding-top:140px;padding-bottom:100px;padding-left:5%;padding-right:5%;background-color:#24262b;background-repeat:no-repeat;background-position:right top">
        <div class="row" style="padding:0 2%">
            <div class="col-12 text-center">
                <div class="column_attr clearfix align_center" style="padding:0 10%;">
                    <h2 style="color:#e0e2e6;">{{getConstField($fields->head4)}}</h2></div>
            </div>
            <div class="col-12">
                <hr class="no_line" style="margin: 0 auto 60px auto"/>
            </div>
            <div class="col-12 column_image_gallery">
                @include('default.realization.home')
            </div>
            <div class="col-12 text-center">
                <hr class="no_line" style="margin:0 auto 40px">
                <h3><a href="{{route('realization.index')}}" style="color:#e0e2e6;">Zobacz więcej zdjęć <span style="font-size:20px;">&#x2E3B;</span></a>
                </h3></div>
        </div>
    </div>
    <div class="section" style="background-color:#24262b;">
        <div class="row">
            <div class="col-12">
                <div class="image_frame image_item no_link scale-with-grid stretch no_border">
                    <div class="image_wrapper"><img class="scale-with-grid"
                                                    src="{{asset('content/mechanic7/images/mechanic7-contact-pic1.png')}}"
                                                    alt="mechanic7-contact-pic1" title="" width="1920" height="497"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="contact"
         style="padding-top:40px; padding-bottom: 40px ;background-color:#24262b;background-repeat:no-repeat;background-position:left top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="mobile_align_center">
                        <h6 style="color:#ecedf0; text-align: center">{{$fields->head5}}</h6>
                        <hr class="no_line" style="margin: 0 auto 15px auto"/>
                        <h2 style="color:#ecedf0; text-align: center">{{$fields->head6}}</h2></div>
                </div>
                <div class="col-12">
                    <hr class="no_line" style="margin: 0 auto 60px auto"/>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="map">
                        {!! getConstField('google_map_iframe') !!}
                    </div>
                </div>
                <div class="col-md-8" style="padding:0 2%">
                    <div class="row">
                        <div class="col-12">
                            <div id="contactWrapper">
                                <div id="contactform">
                                    @include('default.form.contact_form')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12">
                    <hr class="no_line" style="margin: 0 auto 40px auto"/>
                </div>
                @if(!empty(getConstField('facebook')))
                    <div class="col-md-3">
                        @else
                            <div class="col-md-4">
                                @endif
                    <div class="mobile_align_center">
                        <div class="image_frame image_item no_link scale-with-grid no_border">
                            <div class="image_wrapper"><img class="scale-with-grid" style="display: block; margin: 0 auto; width: 36px"
                                                            src="{{asset('image/place.png')}}"
                                                            alt="" title=""/></div>
                        </div>
                        <hr class="no_line" style="margin: 0 auto 15px auto"/>
                        <h5 style="color:#ecedf0; text-align: center">
                            {{getConstField('company_name')}} <br> <br>
                            {{getConstField('company_address')}}, <br>
                            {{getConstField('company_post_code')}} {{getConstField('company_city')}}
                        </h5></div>
                </div>
                @if(!empty(getConstField('facebook')))
                    <div class="col-md-3">
                        @else
                            <div class="col-md-4">
                                @endif
                    <div class="mobile_align_center">
                        <div class="image_frame image_item no_link scale-with-grid no_border">
                            <div class="image_wrapper"><img class="scale-with-grid" style="display: block; margin: 0 auto; width: 36px"
                                                            src="{{asset('image/contact.png')}}"
                                                            alt="" title=""/></div>
                        </div>
                        <hr class="no_line" style="margin: 0 auto 15px auto"/>
                        <a href="Tel:{{str_replace(' ', '', getConstField('phone'))}}" style="text-align: center"><h5 style="color:#ecedf0;">
                                {{getConstField('phone')}}
                            </h5></a>
                        <a href="Mailto:{{getConstField('email')}}" style="text-align: center"><h5 style="color:#ecedf0;">
                                {{getConstField('email')}}
                            </h5></a>
                </div>
            </div>
                @if(!empty(getConstField('facebook')))
                    <div class="col-md-3">
                        @else
                            <div class="col-md-4">
                                @endif
                <div class="mobile_align_center">
                    <div class="image_frame image_item no_link scale-with-grid no_border">
                        <div class="image_wrapper">
                            <img class="scale-with-grid" style="display: block; margin: 0 auto; width: 36px"
                                 src="{{asset('image/check.png')}}"
                                 alt="" title=""/>
                        </div>
                    </div>
                    <hr class="no_line" style="margin: 0 auto 15px auto"/>
                    <h5 style="color:#ecedf0; text-align: center">
                    NIP: {{getConstField('company_nip')}}
                    </h5></div>
            </div>
            @if(!empty(getConstField('facebook')))
            <div class="col-md-3">
                <div class="mobile_align_center">
                    <div class="image_frame image_item no_link scale-with-grid no_border">
                            <div class="image_wrapper">
                                <a href="{{getConstField('facebook')}}">
                                <img class="scale-with-grid" style="display: block; margin: 0 auto; width: 36px"
                                     src="{{asset('image/facebook.png')}}"
                                     alt="" title=""/>
                                </a>
                            </div>
                    </div>
                    <hr class="no_line" style="margin: 0 auto 15px auto"/>
                    <h5 style="color:#ecedf0; text-align: center">
                        Media społecznościowe
                    </h5></div>
            </div>
                            @endif
        </div>
    </div>
@endsection
