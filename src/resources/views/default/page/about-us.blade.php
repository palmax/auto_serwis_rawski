@extends('default.layout')
@section('content')


    <div class="subHeader">
        <img src="{{asset('image/subHeader.png')}}" alt="">
        <div class="container">
            <h1 class="subHeader__title">{{$page->name}}</h1>
        </div>
    </div>

    <div class="section"
         style="padding-top:100px; padding-bottom: 100px;">
        <div class="container">
            {!! $fields->Content !!}
        </div>
    </div>

    <div class="section"
         style="padding-bottom: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="padding:0 2% 0 0">
                    <div class="image_frame image_item no_link scale-with-grid no_border">
                        <div class="image_wrapper"><img class="scale-with-grid"
                                                        src="{{asset('image/laser_gold_mark.jpg')}}"
                                                        alt="mechanic7-about-pic1" title="" width="780" height="953"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="padding:10% 2%">
                    <div class="row">
                        <div class="col-12">
                            <div class="mobile_align_center">
                                <div class="google_font"
                                     style="font-family:'Kanit',Arial,Tahoma,sans-serif;font-size:90px;line-height:90px;font-weight:900;letter-spacing:0px;color:#1955ff;"></div>
                                <hr class="no_line" style="margin: 0 auto 15px auto"/>
                                <h3>{{$fields->head1}}</h3>
                                {!! $fields->text1 !!}
                                <hr class="no_line" style="margin: 0 auto 15px auto"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
