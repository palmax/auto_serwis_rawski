<?php


namespace App\Http\Controllers;


use App\Forms\Admin\PageForm;
use App\Helpers\SeoHelper;
use App\Models\GalleryItem;
use App\Models\Realization;
use App\Models\Page;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class RealizationController extends Controller
{
    public function show($item) {
        SeoHelper::setSeo($item->seo);

        return view('default.realization.show', compact('item'));
    }

    public function home($view){
        $items = GalleryItem::with([])
            ->where('gallery_id', 1)
            ->where('type', 'cover')
            ->where('active', 1)
            ->limit(3)
            ->get();

        $view->items = $items;
    }

    public function index($view) {
        $items = GalleryItem::with([])
            ->where('gallery_id', 1)
            ->where('active', 1)
            ->get();
        $view->items = $items;
    }
}
