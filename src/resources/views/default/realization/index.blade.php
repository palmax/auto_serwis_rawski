@extends('default.layout')
@section('content')
<div class="subHeader">
    <img src="{{asset('image/subHeader.png')}}" alt="">
    <div class="container">
        <h1 class="subHeader__title">{{$page->name}}</h1>
    </div>
</div>

<div class="section galleryIndex" id="gallery" style="padding-top:140px;padding-bottom:140px;padding-left:5%;padding-right:5%;background-color:#24262b;background-repeat:no-repeat;background-position:right top" data-id="#gallery">
    <div class="row" style="padding:0 2%">
        <div class="col-12 column_image_gallery">
            <div id="sc_gallery-5381" class="gallery galleryid-45 gallery-columns-3 gallery-size-full file">
                @foreach($items as $item)
                    <dl class="gallery-item"> <dt class="gallery-icon landscape">
                            <div class="image_frame scale-with-grid"><div class="image_wrapper"><a href="{{renderImage($item->url, 1920, 1080, 'resize')}}" rel="prettyphoto[gallery]" data-rel="prettyphoto[gallery]"><div class="mask"></div><img width="780" height="780" src="{{renderImage($item->url, 780, 780, 'fit')}}" class="attachment-full size-full" alt="" loading="lazy" style="height: auto; width: 100%;"></a></div></div>
                        </dt> </dl>
                @endforeach


            </div>
        </div>
    </div>
</div>
@endsection
