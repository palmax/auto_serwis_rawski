<div id='sc_gallery-5381' class='gallery galleryid-45 gallery-columns-3 gallery-size-full file'>
    @foreach($items as $item)
        <dl class='gallery-item'> <dt class='gallery-icon landscape'>
                <a href='{{renderImage($item->url, 1920, 1080, 'resize')}}'><img width="780" height="780" src="{{renderImage($item->url, 780, 780, 'fit')}}" class="attachment-full size-full" alt="" loading="lazy" /></a>
            </dt> </dl>
    @endforeach
</div>
